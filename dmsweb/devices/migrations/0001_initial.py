# Generated by Django 3.2.5 on 2021-08-02 02:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Devices',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('device_name', models.CharField(max_length=255, unique=True)),
                ('device_type', models.CharField(max_length=255)),
                ('is_active', models.BooleanField(default=True)),
                ('decription', models.CharField(max_length=255)),
                ('image', models.ImageField(null=True, upload_to='devicesimg')),
                ('created_at', models.DateField(auto_now=True)),
                ('updated_at', models.DateField(null=True)),
            ],
        ),
    ]
