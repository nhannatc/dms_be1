from django.db import models

# Create your models here.
class Devices(models.Model):
    device_name = models.CharField(null = False, max_length=255, unique=True)
    device_type = models.CharField(null = False, max_length=255)
    is_active = models.BooleanField(null= False, default=True)
    decription = models.CharField(null = False, max_length=255)
    image = models.ImageField(null = True, upload_to='devicesimg')
    created_at = models.DateField(null = False, auto_now=True)
    updated_at = models.DateField(null = True)

def __str__(self):
        return self.device_name    
