from rest_framework import serializers
from devices.models import Devices

class AddDevicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devices
        fields = ('device_name','device_type','is_active','decription')

class ListDevicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devices
        fields = ('__all__')

class InfoDevicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devices
        fields = ('device_name','device_type','is_active','decription')

