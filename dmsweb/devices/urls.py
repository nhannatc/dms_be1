from django.urls import path
from . import views 
from .views import AddDevices, DevicesListAll, DevicesListFree, UpdateDevices,SearchDevices,ViewDevice

urlpatterns = [
    path('manage/adddevices', AddDevices.as_view()),
    path('manage/updatedevice', UpdateDevices.as_view()),
    path('manage/viewdevice/<int:pk>', ViewDevice.as_view()),
    path('manage/list', DevicesListAll.as_view()),
    path('manage/search/', SearchDevices.as_view()),
    path('availabledevices', DevicesListFree.as_view()),
]