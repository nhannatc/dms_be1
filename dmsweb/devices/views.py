from django.shortcuts import render
from .models import Devices
from users.models import Users
from users import models
from devices import models
from datetime import datetime
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView
from django.db.models import Q
import jwt, datetime, json
from rest_framework.views import APIView
from rest_framework.response import Response
from devices.serializer import AddDevicesSerializer, ListDevicesSerializer, InfoDevicesSerializer
from rest_framework import status, views, permissions, generics
from rest_framework.response import Response




# Create your views here.
class AddDevices(APIView):
    def post(self, request):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        if payload['is_admin']== True:
            serializer = AddDevicesSerializer(data= request.data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse({
                    'message': 'Create a new Devices successful!'
                }, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status =status.HTTP_400_BAD_REQUEST)
        return AuthenticationFailed('Unauthenticated!')

class UpdateDevices(APIView):
    def post(self, request):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        if payload['is_admin']== True:
            payload1 = json.loads(request.body)
            devices = Devices.objects.filter(id=payload1['id']).first()
            device_name = request.data['device_name']
            device_type = request.data['device_type']
            is_active = request.data['is_active']
            decription = request.data['decription']
            devices.updated_at= datetime.datetime.now()
            devices.device_name= device_name
            devices.device_type= device_type
            devices.is_active= is_active
            devices.decription= decription
            devices.save()
            return JsonResponse({
                    'message': 'Update successful!'
                }, status=status.HTTP_200_OK)

        return AuthenticationFailed('Unauthenticated!')

class DevicesListAll(generics.ListCreateAPIView):
    queryset = models.Devices.objects.all()
    serializer_class = ListDevicesSerializer

class DevicesListFree(generics.ListCreateAPIView):
    queryset = models.Devices.objects.filter(is_active=True).all()
    serializer_class = ListDevicesSerializer

class SearchDevices(generics.ListAPIView):
    serializer_class = InfoDevicesSerializer

    def get_queryset(self):
        if self.request.method == 'GET':
            queryset = Devices.objects.all()
            device = self.request.query_params.get('device', None)
            if device is not None:
                d1=device
                d2=device
                queryset = queryset.filter(Q(device_name__contains=d1) | Q(device_type__contains=d2))
            return queryset

class ViewDevice(APIView):
    def get(self, request, *args, **kwargs):
        device = get_object_or_404(Devices, id=kwargs.get('pk'))
        serializer = InfoDevicesSerializer(device)
        return Response(serializer.data,status=status.HTTP_200_OK)