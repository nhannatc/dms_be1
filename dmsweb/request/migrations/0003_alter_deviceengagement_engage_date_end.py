# Generated by Django 3.2.5 on 2021-08-10 05:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0002_deviceengagement'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deviceengagement',
            name='engage_date_end',
            field=models.DateField(null=True),
        ),
    ]
