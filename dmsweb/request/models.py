from django.db import models
from users.models import Users
from devices.models import Devices

# Create your models here.
class DeviceRequest(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE, null=False)
    device = models.ForeignKey(Devices, on_delete=models.CASCADE, null=False)
    request_status = models.CharField(null = False, max_length=255, default="new")
    created_at = models.DateField(null = False, auto_now=True)
    updated_at = models.DateField(null = True)
    date_start = models.DateField(null = True)
    date_end = models.DateField(null = True)

class DeviceEngagement(models.Model):
    device_request = models.ForeignKey(DeviceRequest, on_delete=models.CASCADE, null=False)
    user_accept = models.ForeignKey(Users, on_delete=models.CASCADE, null=False)
    created_at = models.DateField(null = False, auto_now=True)
    updated_at = models.DateField(null = True)
    username = models.CharField(null = True, max_length=255)
    devicename = models.CharField(null = True, max_length=255)
    engage_date_start = models.DateField(null = False, auto_now=True)
    engage_date_end = models.DateField(null = True)
