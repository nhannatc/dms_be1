from rest_framework import serializers
from request.models import DeviceRequest, DeviceEngagement
from devices.models import Devices
from devices.serializer import InfoDevicesSerializer
from users.serializer import UserUpdateSerializer
#from .serializer import EngagementTKSerializer

class DeviceRQSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceRequest
        fields = ('user', 'device')

class RQListSerializer(serializers.ModelSerializer):
    device= InfoDevicesSerializer()
    user= UserUpdateSerializer()
    class Meta:
        model = DeviceRequest
        fields = ('id','user', 'device')

class TrackingSerializer(serializers.ModelSerializer):
    device_request=RQListSerializer()
    user_accept= UserUpdateSerializer()
    class Meta:
        model = DeviceEngagement
        fields = ('device_request','user_accept','engage_date_start','engage_date_end')

class EngagementSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceEngagement
        fields = ('device_request','user_accept')

class EngagementListSerializer(serializers.ModelSerializer):
    user_accept= UserUpdateSerializer()
    class Meta:
        model = DeviceEngagement
        fields = ('id', 'device_request','user_accept','username','devicename','engage_date_start')


class TrackingListSerializer(serializers.ModelSerializer):
    user_accept= UserUpdateSerializer()
    class Meta:
        model = DeviceEngagement
        fields = ('id', 'device_request','user_accept','username','devicename','engage_date_start','engage_date_start')