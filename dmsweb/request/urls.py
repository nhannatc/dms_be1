from . import views
from django.urls import path
from .views import CreateRequest, RequestList, RequestHandle, EngagementList, GiveDeviceBack, TrackingDevices, UpdateDeleteView, SearchRequest, SearchEngagement

urlpatterns = [
        path('create', CreateRequest.as_view()),
        path('list', RequestList.as_view()),
        path('handle', RequestHandle.as_view()),
        path('engagementlist', EngagementList.as_view()),
        path('giveback', GiveDeviceBack.as_view()),
        path('tracking', TrackingDevices.as_view()),
        path('delete/<int:pk>', UpdateDeleteView.as_view()),
        path('searchrequest', SearchRequest.as_view()),
        path('searchengagement', SearchEngagement.as_view()),
]