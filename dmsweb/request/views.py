from django.shortcuts import render
from rest_framework.views import APIView
from request.models import DeviceRequest, DeviceEngagement
from devices.models import Devices
from users.models import Users
import jwt, datetime, json
from rest_framework.response import Response
from rest_framework import status, views, permissions, generics
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.db.models import Q
from request.serializer import DeviceRQSerializer, RQListSerializer, EngagementSerializer, EngagementListSerializer
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView


# Create your views here.
class CreateRequest(APIView):
    def post(self, request, *args, **kwargs):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        #serializer = AddUserSerializer(data= request.data)
        #serializer = DeviceRQSerializer(devicerq)
        payload1 = json.loads(request.body)

        try:
            devices1 = Devices.objects.filter(id=payload1["id"]).first()
            rq = DeviceRequest.objects.create(
                user=user1,
                device=devices1,
            )
            #print(rq.data)
            serializer = DeviceRQSerializer(rq)
            return JsonResponse({'Requesting': serializer.data}, safe=False, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
        except Exception:
            return JsonResponse({'error': 'Something terrible went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class RequestList(generics.ListCreateAPIView):
    queryset = DeviceRequest.objects.filter(request_status="new").all()
    serializer_class = RQListSerializer

class EngagementList(generics.ListCreateAPIView):
    queryset = DeviceEngagement.objects.filter(engage_date_end=None).all()
    serializer_class = EngagementListSerializer

class RequestHandle(APIView):
    def post(self, request, *args, **kwargs):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        payload1 = json.loads(request.body)
        request1 = DeviceRequest.objects.filter(id=payload1['id']).first()
        if request1.request_status != "new":
            return Response({'Error! this request have been handled'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        if payload['is_admin']== True:
            handle = payload1['request_status']
            if handle == "accept":
                id1 = request1.device.id
                id2 = request1.user.id
                device1 = Devices.objects.filter(id=id1).first()
                user2 = Users.objects.filter(id=id2).first()
                device1.is_active = False
                device1.save()
                request1.request_status = payload1['request_status']
                request1.save()
                engage = DeviceEngagement.objects.create(
                    device_request = request1,
                    user_accept = user1,
                    engage_date_start = datetime.datetime.now(),
                    updated_at = datetime.datetime.now(),
                    username= user2.fullname,
                    devicename = device1.device_name
                )
                serializer = EngagementSerializer(engage)
                return Response({'Accepted!'}, status=status.HTTP_200_OK)
            if handle == "decline":
                request1.request_status = payload1['request_status']
                request1.save()
                return Response({'Declined!'}, status=status.HTTP_200_OK)
            else:
                return Response({'Error!'}, status=status.HTTP_404_NOT_FOUND)
        return Response({'error': 'Something terrible went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GiveDeviceBack(APIView):
    def post(self, request, *args, **kwargs):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        payload1 = json.loads(request.body)
        engage1 = DeviceEngagement.objects.filter(id=payload1['id']).first()
        if engage1.engage_date_end is not None:
            return Response({'Error! this request have been handled'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        if payload['is_admin']== True:
            id1 = engage1.device_request.device.id
            device1 = Devices.objects.filter(id=id1).first()
            device1.is_active = True
            device1.save()
            engage1.engage_date_end = datetime.datetime.now()
            engage1.updated_at = datetime.datetime.now()
            engage1.save() 
            return Response({'ok!'}, status=status.HTTP_200_OK)
        return Response({'Error!'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class TrackingDevices(generics.ListAPIView):
    serializer_class = EngagementListSerializer
    def post(self, request, *args, **kwargs):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        payload1 = json.loads(request.body)
        if payload['is_admin']== True:
            start_date = payload1['engage_date_start']
            end_date = payload1['engage_date_start']
            queryset = DeviceEngagement.objects.filter(engage_date_start__gte=start_date, updated_at__lte=end_date)
            serializer = EngagementListSerializer(queryset, many=True)
            return Response(serializer.data)
        return Response({'Error!'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get_queryset(self):
        #serializer_class = EngagementListSerializer
        queryset = DeviceEngagement.objects.all().order_by('-id')
        return queryset

class UpdateDeleteView(RetrieveUpdateDestroyAPIView):
    model = DeviceEngagement
    
    def delete(self, request, *args, **kwargs):
        car = get_object_or_404(DeviceEngagement, id=kwargs.get('pk'))
        car.delete()

        return JsonResponse({
            'message': 'Delete successful!'
        }, status=status.HTTP_200_OK)

class SearchEngagement(generics.ListAPIView):
    serializer_class = EngagementListSerializer

    def get_queryset(self):
        if self.request.method == 'GET':
            queryset = DeviceEngagement.objects.all()
            search = self.request.query_params.get('search', None)
            if search is not None:
                u1=search
                u2=search
                queryset = queryset.filter(Q(username__contains=u1) | Q(devicename__contains=u2))
            return queryset
            

class SearchRequest(generics.ListAPIView):
    serializer_class = RQListSerializer

    def get_queryset(self):
        if self.request.method == 'GET':
            queryset = DeviceRequest.objects.all()
            search = self.request.query_params.get('search', None)
            u1=Users.objects.filter(deleted_at= None).all()
            u2=Devices.objects.all()
            if search is not None:
                user = u1.filter(Q(fullname__contains=search) | Q(username__contains=search)).first()
                device = u2.filter(Q(device_name__contains=search) | Q(device_type__contains=search)).first()
                queryset = queryset.filter(Q(user=user) | Q(device=device))
            return queryset