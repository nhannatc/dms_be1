from django.db import models
from django.contrib.auth.models import AbstractUser
from rest_framework_simplejwt.tokens import RefreshToken


# Create your models here.


class Users(AbstractUser):
    username = models.CharField(null = False, max_length=255, unique=True)
    password = models.CharField(null = False, max_length=255,)
    fullname = models.CharField(null = False, max_length=255)
    image = models.ImageField(null = True, upload_to='userimg')
    created_at = models.DateField(null = False, auto_now=True)
    updated_at = models.DateField(null = True)
    deleted_at = models.DateField(null = True)
    is_admin = models.BooleanField(null= False, default=False)
    refresh_token = models.CharField(max_length=255, null = True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.fullname

    """def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }"""
