from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken, TokenError


from users.models import Users

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('username', 'fullname','is_admin','refresh_token')


class AddUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('username','password', 'fullname','is_admin')

class ListUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields =('id','username','fullname','is_admin','image','created_at','updated_at','deleted_at')

class SearchUser(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('username','fullname','is_admin','created_at','updated_at','deleted_at')

class UserInforSerializer(serializers.ModelSerializer):
    username = serializers.CharField()

class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('username', 'fullname')

class PasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_message = {
        'bad_token': ('Token is expired or invalid')
    }

"""
    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):

        try:
            RefreshToken(self.token).blacklist()

        except TokenError:
            self.fail('bad_token')"""