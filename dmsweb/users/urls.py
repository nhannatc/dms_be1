from . import views
from .views import  LogoutView, ListUsers, UserUpdate, LoginView, UserView, AddUser, ChangePassword, DeleteUser, ChangeUserInfor, LogoutAPIView, ViewUser, SearchUser, TrackingUser, UserEngagement
from rest_framework_simplejwt import views as jwt_view
from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)

urlpatterns = [
    path('create', views.ListCreateUserView.as_view()),
    path('loginapi', LoginView.as_view()),
    path('userview', UserView.as_view()),
    path('manage/adduser', AddUser.as_view()),
    path('manage/updateuser', ChangeUserInfor.as_view()),
    path('changepassword', ChangePassword.as_view()),
    path('manage/deleteuser', DeleteUser.as_view()),
    path('manage/viewuser/<int:pk>', ViewUser.as_view()),
    path('manage/search/', SearchUser.as_view()),
    path('manage/tracking/', TrackingUser.as_view()),
    path('usertracking', UserEngagement.as_view()),
    path('userupdate', UserUpdate.as_view()),
    path('logout', LogoutView.as_view()),
    path('logoutapi', LogoutAPIView.as_view()),
    path('list',ListUsers.as_view()),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]