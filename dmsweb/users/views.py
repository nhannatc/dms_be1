from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from rest_framework import status, views, permissions, generics
from rest_framework.parsers import JSONParser
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, GenericAPIView
from .models import Users
from django.db.models import Q
from request.models import DeviceRequest, DeviceEngagement
from request.serializer import RQListSerializer, TrackingSerializer
from django.contrib.auth.hashers import make_password
from users.serializer import UserSerializer, ListUserSerializer, UserInforSerializer, UserUpdateSerializer, PasswordSerializer, LogoutSerializer,AddUserSerializer, SearchUser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework import authentication
#from rest_framework import filters
#from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth import authenticate
from django.views.generic import ListView
from users import models
from datetime import datetime
import jwt, datetime, json




# Create your views here.
class ListCreateUserView(ListCreateAPIView):
    model = Users
    serializers_class = UserSerializer
    def get_queryset(self):
        return Users.objects.all()
    
    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data= request.data)

        if serializer.is_valid():
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
            serializer.save()
            return JsonResponse({
                'message': 'Create a new User successful!'
            }, status=status.HTTP_201_CREATED)

        return JsonResponse({
            'message': 'Create a new User unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

class UpdateDeleteCarView(RetrieveUpdateDestroyAPIView):
    model = Users
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        Users = get_object_or_404(Users, id=kwargs.get('pk'))
        serializer = UserSerializer(post, data=request.data)

        if serializer.is_valid():
            serializer.save()

            return JsonResponse({
                'message': 'Update User successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Update User unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        Users =  models.Users.objects.get(id=kwargs.get('pk'))
        Users.delete()
        ###
        

        return JsonResponse({
            'message': 'Delete User successful!'
        }, status=status.HTTP_200_OK)

class UserUpdate(APIView):

    def put(self, request):
        token = request.headers["Authorization"]
        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')

        user = Users.objects.filter(id=payload['id']).first()
        serializer = UserSerializer(data=request.data)
        serializer.is_valid()
        fullname = request.data['fullname']
        image = request.data['image']
        user.updated_at= datetime.datetime.now()
        user.fullname = fullname
        user.image = image
        user.save()

        
        return Response(status=status.HTTP_200_OK)

class LoginView(APIView):
    permission_classes=()
    def post(self, request):
        username = request.data['username']
        password = request.data['password']

        user = Users.objects.filter(username=username).first()

        if user is None:
            raise AuthenticationFailed('User not found!')

        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect password!')
        if user.deleted_at is not None:
            raise AuthenticationFailed('User not found!')
        payload = {
            'id': user.id,
            'is_admin': user.is_admin,
            'fullname':user.fullname,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=1440),
            'iat': datetime.datetime.utcnow()
        }

        token = jwt.encode(payload, 'secret', algorithm='HS256').decode('utf-8')

        response = Response()

        #response.set_cookie(key='jwt', value=token, httponly=True)
        response.data = {
            'jwt': token
        }
        return response

class UserView(APIView):

    def get(self, request):
        #token = request.COOKIES.get('jwt')
        #print(request.headers["Authorization"])
        token =request.headers["Authorization"]
        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')

        user = Users.objects.filter(id=payload['id']).first()
        if token == user.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        serializer = UserSerializer(user)
        return Response(serializer.data)

class LogoutView(APIView):
    def post(self, request):
        response = Response()
        response.delete_cookie('jwt')
        response.data = {
            'message': 'success'
        }
        return response

class ListUsers(generics.ListCreateAPIView):
    
    queryset = models.Users.objects.filter(deleted_at__isnull=True).all()
    serializer_class = ListUserSerializer


class AddUser(APIView):
    def post(self, request):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        if payload['is_admin']== True:
            serializer = AddUserSerializer(data= request.data)
            if serializer.is_valid():
                serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
                serializer.save()
                return JsonResponse({
                    'message': 'Create a new User successful!'
                }, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status =status.HTTP_400_BAD_REQUEST)
        return AuthenticationFailed('Unauthenticated!')

class DeleteUser(APIView):
    def put(self, request):
        token = request.headers["Authorization"]
        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        if payload['is_admin']== True:
            payload1 = json.loads(request.body)
            user = Users.objects.filter(id=payload1['id']).first()
            user.deleted_at= datetime.datetime.now()
            user.save()
            return Response({'This user have been deleted'},status=status.HTTP_200_OK)

class ChangePassword(APIView):
    def put(self, request):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user = Users.objects.filter(id=payload['id']).first()
        if token == user.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        old_password = request.data['old_password']
        new_password = request.data['new_password']
        serializer =PasswordSerializer(data= request.data)
        if serializer.is_valid():
            if not user.check_password(old_password):
                return Response({'old_password': ['Wrong password.']},status=status.HTTP_400_BAD_REQUEST)
                
                
            # set_password also hashes the password that the user will get
            n = make_password(new_password)
            user.password = n
            user.save()
            return Response({'status': 'password set'},status=status.HTTP_200_OK)

        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        
class ChangeUserInfor(APIView):
    def post(self, request):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user1 = Users.objects.filter(id=payload['id']).first()
        if token == user1.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        if payload['is_admin']== True:
            payload1 = json.loads(request.body)
            user = Users.objects.filter(id=payload1['id']).first()
            username = request.data['username']
            fullname = request.data['fullname']
            is_admin = request.data['is_admin']
            image = request.data['image']
            user.updated_at= datetime.datetime.now()
            user.username = username
            user.fullname = fullname
            user.is_admin = is_admin
            user.image = image
            user.save()

            return Response({'update successful'},status=status.HTTP_200_OK)
        return Response({'Unauthenticated!'})

class LogoutAPIView(generics.GenericAPIView):
    serializer_class = LogoutSerializer

    #permission_classes = (permissions.IsAuthenticated,)
    
    def post(self, request):
        token = request.headers["Authorization"]

        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user = Users.objects.filter(id=payload['id']).first()
        print(user.refresh_token)
        if token == user.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        user.refresh_token= token
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
        
class ViewUser(APIView):
    def get(self, request, *args, **kwargs):
        user = get_object_or_404(Users, id=kwargs.get('pk'))
        serializer = ListUserSerializer(user)
        return Response(serializer.data,status=status.HTTP_200_OK)

class SearchUser(generics.ListAPIView):
    serializer_class = SearchUser

    def get_queryset(self):
        if self.request.method == 'GET':
            queryset = Users.objects.filter(deleted_at__isnull=True).all()
            username = self.request.query_params.get('name', None)
            if username is not None:
                u1=username
                u2=username
                queryset = queryset.filter(Q(username__contains=u1) | Q(fullname__contains=u2))
            return queryset




class TrackingUser(generics.ListAPIView):
    serializer_class = TrackingSerializer
    def get_queryset(self):
        if self.request.method == 'GET':
            queryset = DeviceEngagement.objects.all()
            user = self.request.GET.get('id', None)
            if user is not None:
                RQ = Users.objects.filter(id=user).first()
            queryset = DeviceEngagement.objects.filter(username=RQ)
            return queryset

class UserEngagement(APIView):
    def get(self, request):
        token =request.headers["Authorization"]
        if not token:
            raise AuthenticationFailed('Unauthenticated!')
        try:
            payload = jwt.decode(token, 'secret', algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        user = Users.objects.filter(id=payload['id']).first()
        RQ = DeviceEngagement.objects.filter(username=user)
        if token == user.refresh_token:
            raise AuthenticationFailed('Unauthenticated!')
        serializer = TrackingSerializer(RQ, many=True)
        return Response(serializer.data)